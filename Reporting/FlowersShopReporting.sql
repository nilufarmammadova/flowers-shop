select count(customer_id) as customer_count from customers ;
select count(*) from orders o 
join customers c on o.customer_id = c.customer_id
join branches b on o.branch_id = b.branch_id;

select count (order_id), order_date as number_of_orders
from orders
group by order_date
order by order_date;

select name from flowers f
inner join order_details d on d.flower_id = f.flower_id
order by quantity desc;

SELECT DISTINCT f.name, s.name
FROM flowers f
INNER JOIN suppliers s ON f.supplier_id = s.supplier_id;

select distinct s.name, f.name
from suppliers s
inner join flowers f on f.supplier_id = s.supplier_id;

select b.branch_id, count(o.order_id) as order_number from branches b
inner join orders o on o.branch_id = b.branch_id
inner join order_details d on d.order_id = o.order_id
group by b.branch_id
order by order_number desc; 
